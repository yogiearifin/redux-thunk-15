import React from "react";
import styles from "./assets/navbar.module.scss";

export default function Navbar() {
  return (
    <div className={styles.container}>
      <div className={styles.logo}>
        <a href="/">
          <h1>My Event</h1>
        </a>
      </div>
      <div className={styles.containerLogin}>
        <p>Login</p>
        <p>Register</p>
      </div>
    </div>
  );
}
