import React from "react";
import styles from "./assets/card.module.scss";

export default function Card({ ...props }) {
  const { id, title, image } = props;
  return (
    <a href={`event/${id}`}>
      <div className={styles.container}>
        <img src={image} alt={title} />
        <p>
          <b>{title}</b>
        </p>
      </div>
    </a>
  );
}
