import { BrowserRouter, Route, Routes } from "react-router-dom";

import Home from "../pages/home/Home";
import Detail from "../pages/detail/Detail";
import NotFound from "../pages/notFound/NotFound";

export default function Routers() {
  return (
    <>
      <BrowserRouter>
        <Routes>
          <Route exact path="/" element={<Home />} />
          <Route exact path="/event/:id" element={<Detail />} />
          <Route path="*" element={<NotFound />} />
        </Routes>
      </BrowserRouter>
    </>
  );
}
