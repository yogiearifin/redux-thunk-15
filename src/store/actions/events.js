import {
  GET_EVENTS_BEGIN,
  GET_EVENTS_SUCCESS,
  GET_EVENTS_FAIL,
  GET_EVENT_DETAIL_BEGIN,
  GET_EVENTS_DETAIL_SUCCESS,
  GET_EVENTS_DETAIL_FAIL,
} from "./types";
import axios from "axios";
import { BASE_URL } from "../../constants/constants";

export const getEvents = () => (dispatch) => {
  dispatch({
    type: GET_EVENTS_BEGIN,
  });
  axios
    .get(BASE_URL)
    .then((res) =>
      dispatch({
        type: GET_EVENTS_SUCCESS,
        payload: res.data,
      })
    )
    .catch((err) => {
      dispatch({
        type: GET_EVENTS_FAIL,
        error: err,
      });
    });
};

export const getEventDetail = (id) => (dispatch) => {
  dispatch({
    type: GET_EVENT_DETAIL_BEGIN,
  });
  axios
    .get(`${BASE_URL}/${id}`)
    .then((res) =>
      dispatch({
        type: GET_EVENTS_DETAIL_SUCCESS,
        payload: res.data,
      })
    )
    .catch((err) => {
      dispatch({
        type: GET_EVENTS_DETAIL_FAIL,
        error: err,
      });
    });
};
