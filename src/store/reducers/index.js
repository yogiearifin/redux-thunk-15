import { combineReducers } from "redux";
import events from "./events";

const rootReducers = combineReducers({ events });

export default rootReducers;
