import {
  GET_EVENTS_BEGIN,
  GET_EVENTS_SUCCESS,
  GET_EVENTS_FAIL,
  GET_EVENT_DETAIL_BEGIN,
  GET_EVENTS_DETAIL_SUCCESS,
  GET_EVENTS_DETAIL_FAIL,
} from "../actions/types";

const initialState = {
  eventList: {
    loading: false,
    error: null,
    eventListData: [],
  },
  detailEvent: {
    loading: false,
    error: null,
    eventDetails: {},
  },
};

const events = (state = initialState, action) => {
  const { type, payload, error } = action;
  switch (type) {
    case GET_EVENTS_BEGIN:
      return {
        ...state,
        eventList: {
          ...state.eventList,
          loading: true,
          error: null,
        },
      };
    case GET_EVENTS_SUCCESS:
      return {
        ...state,
        eventList: {
          ...state.eventList,
          loading: false,
          error: null,
          eventListData: payload,
        },
      };
    case GET_EVENTS_FAIL:
      return {
        ...state,
        eventList: {
          ...state.eventList,
          loading: false,
          error: error,
          eventListData: [],
        },
      };
    case GET_EVENT_DETAIL_BEGIN:
      return {
        ...state,
        detailEvent: {
          ...state.detailEvent,
          loading: true,
          error: null,
        },
      };
    case GET_EVENTS_DETAIL_SUCCESS:
      return {
        ...state,
        detailEvent: {
          ...state.detailEvent,
          loading: false,
          error: null,
          eventDetails: payload,
        },
      };
    case GET_EVENTS_DETAIL_FAIL:
      return {
        ...state,
        detailEvent: {
          ...state.detailEvent,
          loading: false,
          error: error,
          eventDetails: [],
        },
      };
    default:
      return {
        ...state,
      };
  }
};

export default events;
