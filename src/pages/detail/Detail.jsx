import { useEffect } from "react";
import { useDispatch, useSelector } from "react-redux";
import { useParams } from "react-router";
import { getEventDetail } from "../../store/actions/events";

export default function Detail() {
  const { loading, eventDetails } = useSelector(
    (state) => state.events.detailEvent
  );
  console.log(eventDetails);
  const dispatch = useDispatch();
  const { id } = useParams();
  useEffect(() => {
    dispatch(getEventDetail(id));
  }, []);
  return (
    <div>
      <h1>Detail Event</h1>
      {loading ? (
        "loading detail..."
      ) : (
        <div>
          <img src={eventDetails.image} alt={eventDetails.title} />
          <h1>{eventDetails.title}</h1>
          <p>Location: {eventDetails.location}</p>
          <p>City: {eventDetails.city}</p>
          <p>Hosted By: {eventDetails.hosted_by}</p>
          <p>
            Fee:
            {eventDetails.fee === 0 ? "Free" : `${eventDetails.fee} rupiah`}
          </p>
        </div>
      )}
    </div>
  );
}
