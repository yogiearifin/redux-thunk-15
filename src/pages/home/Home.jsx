import { useDispatch, useSelector } from "react-redux";
import { useEffect, useState } from "react";
import { getEvents } from "../../store/actions/events";
import Card from "../../components/card/Card";
import styles from "./assets/home.module.scss";

export default function Home() {
  const [width, setWidth] = useState(window.innerWidth);

  const handleResize = () => {
    setWidth(window.innerWidth);
  };

  useEffect(() => {
    window.addEventListener("resize", handleResize);
    return () => {
      window.removeEventListener("resize", handleResize);
    };
  }, []);

  const { loading, eventListData } = useSelector(
    (state) => state.events.eventList
  );
  console.log("events", eventListData);
  const dispatch = useDispatch();
  useEffect(() => {
    dispatch(getEvents());
  }, []);
  console.log("width", width);
  return (
    <div className={styles.container}>
      {loading
        ? "loading..."
        : eventListData.map((item, index) => {
            return (
              <div key={index}>
                <Card id={item.id} image={item.image} title={item.title} />
              </div>
            );
          })}
      {width <= 600 ? "mobile" : "pc"}
    </div>
  );
}
