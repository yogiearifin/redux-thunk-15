import logo from "./logo.svg";
import "./App.css";
import Routers from "./routes/routes";
import Navbar from "./components/navbar/Navbar";

function App() {
  return (
    <div>
      <Navbar />
      <Routers />
    </div>
  );
}

export default App;
